
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>EXERCISE 3</title>
</head>

<body>

<div class="container d-flex justify-content-center">
    <div class="card mt-5 w-50 border-success">
        <div class="card-header text-center h1 text-white bg-success">
            EXERCISE 3
        </div>
        <div class="card-body">  
            <h5>Email Validation</h5>
            <form method="POST">
                <input type="text" class="form-control" name='email' placeholder="Enter email address"> <br>
                <button type="submit" class="btn btn-primary float-right" name="post">Submit</button>
            </form>
        </div>
    </div>
</div>

<?php

    class validEmail{
        public $email;

        public function __construct(){
            $this->email = isset($_POST['email']) ? $_POST['email'] : null;
        }

        public function validateEmail(){
            if (filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
                echo "<script type='text/javascript'>alert('$this->email is a VALID EMAIL');</script>";
            }
            else if($this->email==""){
                echo "<script type='text/javascript'>alert('Please Enter your email');</script>";
            } else {
                echo "<script type='text/javascript'>alert('$this->email is an INVALID EMAIL');</script>";
            }
        }
    }

    $emailVal = new validEmail();
    $emailVal->validateEmail();

?>


</body>
</html>


