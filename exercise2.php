<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>EXERCISE 2</title>

</head>

<body>


    <div class="container d-flex justify-content-center">
        <div class="card mt-5 w-50 border-success">
            <div class="card-header text-center h1 text-white bg-success">
                EXERCISE 2
            </div>
            <div class="card-body">  
                    <form method="POST">
                    <div class="row">
                        <input type="text" class="form-control w-75 mr-2" name="input" placeholder="Enter data here">
                        <input type="submit" name="submit" value="Add" class="btn btn-primary login_btn">
                    </div>
                </form>
            <br>
            <?php
            class userInput{
            
                public $userInput;

                function __construct($userInput){
                    $this->userInput = $userInput;
                }

                function addInput(){
                    if(!isset($_POST['option'])){
                        $_POST['option'] = [];
                    }

                    array_push($_POST['option'],$this->userInput);
                    
                    echo '<select name="dropdown">';
                            
                    foreach ($_POST['option'] as $value) {
                        echo '<option value="'.$value.'">"'.$value.'"</option>' ;
                    }
                    echo '</select>';
                }
            }
            if(isset($_POST['submit'])){
                $userInput = $_POST['input'];
                $input = new userInput($userInput);
                $input->addInput();
            }
            ?>
            </div>
        </div>
    </div>

     



</body>

</html>