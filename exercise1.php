<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>EXERCISE 4</title>
</head>
<body>
    
<div class="container d-flex justify-content-center">
    <div class="card mt-5 w-50 border-success">
        <div class="card-header text-center h1 text-white bg-success">
            EXERCISE 1
        </div>
        <div class="card-body">  
            <?php

                class Date{

                    public $date1,$date2;

                    public function compareDate(){
                        $date1=new DateTime("2013/09/04");
                        echo "Date 1: ".date_format($date1,"Y/m/d")."<br>";
                        $date2=new DateTime("1981-11-03");
                        echo "Date 2: ".date_format($date2,"Y/m/d")."<br><br><br>";
                        $difference = $date1->diff($date2);

                        echo "Difference: " . $difference->y . " years, " . $difference->m." months, ".$difference->d." days "; 
                    }
                }

                $result= new Date();
                $result->compareDate();

            ?>
        </div>
    </div>
</div>


</body>
</html>
