<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>EXERCISE 4</title>
</head>

<body>

    <div class="container d-flex justify-content-center">
        <div class="card mt-5 w-50 border-success">
            <div class="card-header text-center h1 text-white bg-success">
                EXERCISE 4
            </div>
            <div class="card-body">  
                <form name="form" method="POST" class="m-2">
                    <input id="name" type="text" name="name" placeholder="Name" class="form-control" required><br>
                    <input id="address" type="text" name="address" placeholder=" Address" class="form-control" required><br>
                    <input id="age" type="number" name="age" placeholder="Age" class="form-control" required><br>
                    <input id="contact" type="number" name="contact" placeholder="Contact Number" class="form-control" required><br>
                    <button class="btn btn-primary float-right" name="post">Submit</button>
                </form>
                <br>
                <?php
                    class Validate
                    {


                        public function __construct()
                        {
                            $this->name = isset($_POST['name']) ? $_POST['name'] : null;
                            $this->address = isset($_POST['address']) ? $_POST['address'] : null;
                            $this->age = isset($_POST['age']) ? $_POST['age'] : null;
                            $this->contact = isset($_POST['contact']) ? $_POST['contact'] : null;
                        }

                        public function validateMe(){
                                if (!preg_match("/^[a-zA-z]*$/", $this->name)) {
                                    $Err = " ---- INVALID NAME<br>";
                                    echo $Err;
                                } else if($this->name=="") {
                                    echo $this->name . " ---- ENTER ONLY LETTERS FROM A-Z<br>";
                                }else{
                                    echo $this->name . " ---- NAME IS VALID <br>";
                                }


                                if(!preg_match("/^[a-zA-z]*$/", $this->address)){
                                    echo $this->address ." ---- INVALID ADDRESS<br>";
                                }
                                else if($this->address=="") {
                                    echo $this->address. " ---- ENTER VALID ADDRESS<br>";
                                }else{
                                    echo $this->address ." ---- ADDRESS IS VALID <br>";
                                }
                                

                                if ($this->age <= 18) {
                                    echo $this->age ." ---- AGE MUST BE ABOVE 18 YEARS<br>";
                                } else {
                                    echo $this->age ." ---- IS OF LEGAL AGE<br>";
                                }

                                $length = strlen($this->contact);

                                if (!($length == 11))  {
                                    $Err = $this->contact ." ---- ENTER 11 DIGIT NUMBERS";
                                    echo $Err;
                                } else {
                                    echo $this->contact ." ---- PHONE NUMBER IS VALID";
                                } 
                        }
                    }
                    $validate = new Validate();

                    $validate->validateMe();
                ?>
            </div>
        </div>
    </div>
</body>

</html>